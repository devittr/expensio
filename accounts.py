from datetime import datetime
import csv

class Account:

    ACCOUNT_TYPES = {0: 'Spending', }

    def __init__(self, name, acct_type, start_date, iban):
        self.name = name
        self.acct_type = acct_type
        self.start_date = start_date
        self.iban = iban

    # TODO: Read balance from ledger
    def get_balance(self, asOf=datetime.now()):
        pass


class Payment:

    def __init__(self, acct_from, acct_to, amount, msg, date=datetime.now()):
        self.date_time = date.strftime('%Y-%m-%d %H:%M')
        self.acct_from = acct_from
        self.acct_to = acct_to
        self.amt = float(amount)
        self.msg = msg

    # TODO: load payment to ledger
    def load(self):
        ledger_writer = csv.DictWriter(open('ledger.csv', 'a', newline=''),
                                fieldnames=['date_time', 'acct_from', 'acct_to', 'amt', 'msg'])
        ledger_writer.writerow(self.__dict__)


