from accounts import Payment
from datetime import datetime

LEDGER_FILE = 'ledger.csv'

def load_payment(date, acct_from, acct_to, amt, msg):
    date = datetime.strptime(date, '%Y%m%d')
    p = Payment(acct_from=acct_from, acct_to=acct_to, amount=amt, msg=msg, date=date)
    p.load()

if __name__ == '__main__':
    response = ''
    while response.lower() != 'q':
        response = input('Enter payment: ')
        try:
            date, acct_from, acct_to, amt, msg = response.split()
        except Exception:
            print('Input not recognised')
            continue
        load_payment(date, acct_from, acct_to, amt, msg)

